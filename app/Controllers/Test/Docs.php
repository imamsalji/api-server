<?php

namespace App\Controllers\Test;

use App\Libraries\smsotp;
use CodeIgniter\Controller;
use Dompdf\Dompdf;
use CodeIgniter\API\ResponseTrait;

class Docs extends Controller
{
    use ResponseTrait; 

    public function pdf()
    {
        $response = [
            'Download Pdf' => base_url().'/test/docs/pdf_download',
            'Langsung Pdf' => base_url().'/test/docs/pdf_live',
        ];
        return $this->respond($response);
    }

    public function pdf_download()
    {
        $filename = date('y-m-d-H-i-s'). 'Pdf Dwonload';

        // instantiate and use the dompdf class
        $dompdf = new Dompdf();

		$content1 ='
			<table width="100%" style="vertical-align: bottom; font-family: Times New Roman; font-size: 14px; color: #000000;">
				<tr>
					<td width="10%" align="left">
						Nomor
					</td>
					<td width="85%" align="left">
						:
					</td>
				</tr>
				<tr>
					<td width="10%" align="left">
						Lampiran
					</td>
					<td width="85%" align="left">
						: -
					</td>
				</tr>
				<tr>
					<td width="10%" align="left">
						Perihal
					</td>
					<td width="85%" align="left">
						: <b>Surat Peringatan ke-</b>
					</td>
				</tr>
			</table>
		';
		$content2='
			<ul style="font-family: Times New Roman; font-size: 14px;">
				<li>
					<table>
					<tr>
						<td align="left">
							No Pembiayaan
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							&lt;pinjaman_id&gt;
						</td>
					</tr>
					<tr>
						<td align="left">
							Tagihan per tgl
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							&lt;YYYY-MM-DD&gt;
						</td>
					</tr>
					<tr>
						<td align="left">
							Jumlah tagihan (Rp)
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							Rp.&lt;jdwl_pembayaran_cicilan&gt;,-
						</td>
					</tr>
					<tr>
						<td align="left">
							Jumlah denda (Rp)
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							Rp.&lt;jml_biaya_keterlambatan&gt;,-
						</td>
					</tr>
					<tr>
						<td align="left">
							Jumlah tagihan + denda (Rp)
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							Rp.&lt;total_jml_pembayaran_cicilan&gt;,-
						</td>
					</tr>
				</table>
				</li>
			</ul>
		';

		$html1 = '
				<div style="height:10px;"></div>
				<div style="font-family:tahoma;line-height:20px">
					<p style="text-align:right; font-family: Times New Roman; font-size: 14px;"><b>Information Classification: Confidential</b><br>Jakarta, </p>
					<div style="height:10px;"></div>

					'.$content1.'

					<p style="font-family: Times New Roman; font-size: 14px;">
						Kepada Yth.<br>
						<b></b><br>
						
					</p>

					<p style="text-align:justify; font-family: Times New Roman; font-size: 14px;">
						Dengan hormat,<br>
						Sehubungan dengan penerbitan efek obligasi yang dilakukan pada platform Danamart, 
						untuk itu kami mengingatkan bahwa pembiayaan efek obligasi sudah harus dibayarkan (pokok/imbal hasil) 
						namun berdasarkan catatan kami sampai dengan tanggal surat ini dikeluarkan, 
						Bapak/Ibu (untuk selanjutnya disebut Penerbit) belum melakukan pembayaran dari 
						tanggal  dengan rincian sebagai berikut :
					</p>

					'.$content2.'

					<p style="font-family: Times New Roman; font-size: 14px;margin:0;">
						Pembayaran dapat dilakukan melalui transfer ke rekening :
					</p>

					<table width="100%" style="padding:0;vertical-align: bottom; font-family: Times New Roman; font-size: 14px; color: #000000;">
						<tr>
							<td width="10%" align="left">
								Bank
							</td>
							<td width="85%" align="left">
								: BCA
							</td>
						</tr>
						<tr>
							<td width="10%" align="left">
								No.Rekening 
							</td>
							<td width="85%" align="left">
								: 527 1837 8888
							</td>
						</tr>
						<tr>
							<td width="10%" align="left">
								Atas Nama
							</td>
							<td width="85%" align="left">
								: PT. Dana Aguna Nusantara
							</td>
						</tr>
					</table>

					<p style="text-align:justify; font-family: Times New Roman; font-size: 14px;">
						Untuk menghindari beban kewajiban yang lebih besar karena denda 0,2% per hari dari total 
						kewajiban (pokok/Imbal hasil) yang harus dibayarkan, maka kami mohon kepada Bapak/Ibu untuk 
						segera menyelesaikan seluruh kewajiban pembiayaan dengan Danamart. 
						Jika tidak ada pembayaran maka Danamart akan melakukan <b>Upaya Hukum Baik secara perdata maupun pidana</b>. 
						Atas perhatian dan kerjasamanya selama ini, kami ucapkan terima kasih.
					</p>

					<p style="font-family: Times New Roman; font-size: 14px;margin:0;">
						Hormat kami, <br>
						<b>PT. Dana Aguna Nusantara
							<br><br><br><br>

							<u>Lugina Febriantie</u><br>
							Chief Operation Officer
						</b>
					</p>
				</div>';

        $dompdf->loadHtml($html1);
        $dompdf->setPaper('A4', 'potrait');
        $dompdf->render();
        // download pdf
        $dompdf->stream($filename);
    }

    public function pdf_live()
    {
        $dompdf = new Dompdf();

		$content1 ='
			<table width="100%" style="vertical-align: bottom; font-family: Times New Roman; font-size: 14px; color: #000000;">
				<tr>
					<td width="10%" align="left">
						Nomor
					</td>
					<td width="85%" align="left">
						:
					</td>
				</tr>
				<tr>
					<td width="10%" align="left">
						Lampiran
					</td>
					<td width="85%" align="left">
						: -
					</td>
				</tr>
				<tr>
					<td width="10%" align="left">
						Perihal
					</td>
					<td width="85%" align="left">
						: <b>Surat Peringatan ke-</b>
					</td>
				</tr>
			</table>
		';
		$content2='
			<ul style="font-family: Times New Roman; font-size: 14px;">
				<li>
					<table>
					<tr>
						<td align="left">
							No Pembiayaan
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							&lt;pinjaman_id&gt;
						</td>
					</tr>
					<tr>
						<td align="left">
							Tagihan per tgl
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							&lt;YYYY-MM-DD&gt;
						</td>
					</tr>
					<tr>
						<td align="left">
							Jumlah tagihan (Rp)
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							Rp.&lt;jdwl_pembayaran_cicilan&gt;,-
						</td>
					</tr>
					<tr>
						<td align="left">
							Jumlah denda (Rp)
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							Rp.&lt;jml_biaya_keterlambatan&gt;,-
						</td>
					</tr>
					<tr>
						<td align="left">
							Jumlah tagihan + denda (Rp)
						</td>
						<td  align="left">
							: 
						</td>
						<td  align="left">
							Rp.&lt;total_jml_pembayaran_cicilan&gt;,-
						</td>
					</tr>
				</table>
				</li>
			</ul>
		';

		$html1 = '
				<div style="height:10px;"></div>
				<div style="font-family:tahoma;line-height:20px">
					<p style="text-align:right; font-family: Times New Roman; font-size: 14px;"><b>Information Classification: Confidential</b><br>Jakarta, </p>
					<div style="height:10px;"></div>

					'.$content1.'

					<p style="font-family: Times New Roman; font-size: 14px;">
						Kepada Yth.<br>
						<b></b><br>
						
					</p>

					<p style="text-align:justify; font-family: Times New Roman; font-size: 14px;">
						Dengan hormat,<br>
						Sehubungan dengan penerbitan efek obligasi yang dilakukan pada platform Danamart, 
						untuk itu kami mengingatkan bahwa pembiayaan efek obligasi sudah harus dibayarkan (pokok/imbal hasil) 
						namun berdasarkan catatan kami sampai dengan tanggal surat ini dikeluarkan, 
						Bapak/Ibu (untuk selanjutnya disebut Penerbit) belum melakukan pembayaran dari 
						tanggal  dengan rincian sebagai berikut :
					</p>

					'.$content2.'

					<p style="font-family: Times New Roman; font-size: 14px;margin:0;">
						Pembayaran dapat dilakukan melalui transfer ke rekening :
					</p>

					<table width="100%" style="padding:0;vertical-align: bottom; font-family: Times New Roman; font-size: 14px; color: #000000;">
						<tr>
							<td width="10%" align="left">
								Bank
							</td>
							<td width="85%" align="left">
								: BCA
							</td>
						</tr>
						<tr>
							<td width="10%" align="left">
								No.Rekening 
							</td>
							<td width="85%" align="left">
								: 527 1837 8888
							</td>
						</tr>
						<tr>
							<td width="10%" align="left">
								Atas Nama
							</td>
							<td width="85%" align="left">
								: PT. Dana Aguna Nusantara
							</td>
						</tr>
					</table>

					<p style="text-align:justify; font-family: Times New Roman; font-size: 14px;">
						Untuk menghindari beban kewajiban yang lebih besar karena denda 0,2% per hari dari total 
						kewajiban (pokok/Imbal hasil) yang harus dibayarkan, maka kami mohon kepada Bapak/Ibu untuk 
						segera menyelesaikan seluruh kewajiban pembiayaan dengan Danamart. 
						Jika tidak ada pembayaran maka Danamart akan melakukan <b>Upaya Hukum Baik secara perdata maupun pidana</b>. 
						Atas perhatian dan kerjasamanya selama ini, kami ucapkan terima kasih.
					</p>

					<p style="font-family: Times New Roman; font-size: 14px;margin:0;">
						Hormat kami, <br>
						<b>PT. Dana Aguna Nusantara
							<br><br><br><br>

							<u>Lugina Febriantie</u><br>
							Chief Operation Officer
						</b>
					</p>
				</div>';

        $dompdf->loadHtml($html1);
        $dompdf->setPaper('A4', 'potrait');
        $dompdf->render();
        // pdf show
        $dompdf->stream('contoh dokumen pdf',array(
            "attachment" => false
        ));
    }
}