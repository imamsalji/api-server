<?php

namespace App\Controllers\Test;

use App\Libraries\smsotp;
use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;

class Sms extends Controller
{
    use ResponseTrait; 

    public $sms;

    function __construct(){
        $this->sms = new smsotp;
    }

    public function kode_otp()
    {
        $nohp = $this->request->getPost('nohp');
        $response = $this->sms->otp($nohp);
        return $this->respond($response);
    }

    public function verifikasi_otp()
    {
        $nohp = $this->request->getPost('nohp');
        $request_id = $this->request->getPost('request_id');
        $code = $this->request->getPost('code');
        $response = $this->sms->verifikasi($request_id,$nohp,$code);
        return $this->respond($response);
    }
}