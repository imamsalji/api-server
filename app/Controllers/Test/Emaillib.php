<?php

namespace App\Controllers\Test;

use App\Libraries\Email;
use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;

class Emaillib extends Controller
{
    use ResponseTrait; 

    public $template_mail;

    function __construct(){
        $this->template_mail = new Email;
    }

    public function send_email()
    {
        $content = '
					<tr>
						<td class="container1" colspan="2">
						<p>
							<b>Halo ,</b>
							<br><br>
							Terkait dengan pembiyaan Anda melalui platform Danamart, ini adalah email pengingat pembayaran imbal hasil/dividen yang sudah mernjadi kewajiban Anda, dengan rincian:
							<br><br>
							ID Pembiayaan: 
							<br>
							Jumlah Pembayaran: 
							<br>
							Tanggal Jatuh Tempo Pembayaran: 
							<br><br>
							Maka dari itu, diharapkan Anda harus segera melakukan pembayaran imbal hasil/dividen dengan rincian tersebut di atas dengan tepat waktu untuk menghindari biaya keterlambatan.
							<br><br>
							Untuk melakukan pembayaran, silahkan login ke akun Anda pada platform Danamart. Abaikan email ini jika Anda sudah melakukan pembayaran tersebut.
							<br><br>
							Jika terdapat pertanyaan mengenai produk/layanan Danamart, silahkan hubungi kami di 021 2555 6719.
						</p>
						</td>
					</tr>
					<tr>
						<td class="container1" colspan="2">
						<p>
							Salam,<br>
							<b>Danamart Team</b>
						</p>
						</td>
					</tr>';
          $email = $this->request->getPost('email');
          echo $this->request->getPost('email');
          if (empty($email)) {
              echo 'letakan email di url sebelah send_email/emailanda';
              die();
          }
        $htmlContent = $this->template_mail->template($content);
        $data_email['From'] = 'support@danamart.id';
        $data_email['To'] = $email;
        $data_email['Subject'] = 'percobaan Template';
        $data_email['HtmlBody'] = $htmlContent;
        $data_email['ReplyTo'] = 'support@danamart.id';
        $payload = json_encode($data_email);

        $curl_email = curl_init();
        curl_setopt($curl_email, CURLOPT_URL, "https://api.postmarkapp.com/email");
        curl_setopt($curl_email, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl_email, CURLOPT_ENCODING, "");
        curl_setopt($curl_email, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl_email, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl_email, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl_email, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl_email, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl_email, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "cache-control: no-cache", "X-Postmark-Server-Token: ed81f9b5-4fae-47ef-afcf-3709c2f12fa6"));
        $response = curl_exec($curl_email);
        curl_close($curl_email);
        $json = json_decode($response, true);
        return $this->respond($json);
    }
}