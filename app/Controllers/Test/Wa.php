<?php

namespace App\Controllers\Test;

use App\Libraries\Zenziva;
use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;

class Wa extends Controller
{
    use ResponseTrait; 

    public $wa;

    function __construct(){
        $this->wa = new Zenziva;
    }

    public function reguler()
    {
        $telepon = $this->request->getPost('telepon');
        $message = $this->request->getPost('message');
        $response = $this->wa->send_wa($telepon,$message);
        return $this->respond($response);
    }

    public function center()
    {
        $telepon = $this->request->getPost('telepon');
        $message = $this->request->getPost('message');
        $response = $this->wa->send_wac($telepon,$message);
        return $this->respond($response);
    }
}