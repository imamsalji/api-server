<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Libraries\smsotp;
use App\Libraries\Hybridauth;
use App\Models\ModelAutentikasi;
use App\Models\ModelLogs;
use Exception;

class Autentikasi extends Controller
{
    use ResponseTrait;

    public $sms;
    public $hybridauth;
    public $modelAutentifikasi;
    public $db;

    function __construct(){
        $this->sms = new smsotp;
        $this->hybridauth = new Hybridauth;
        $this->modelAutentifikasi = new ModelAutentikasi;
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $modelAutentifikasi = $this->modelAutentifikasi;
        $validate = \config\Services::validation();
        $rules = [
            'email' => [
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => 'Silahkan Masukan Email',
                ]
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Silahkan Masukan Password'
                ]
            ],
        ];
        
        $validate->setRules($rules);
        if (!$validate->withRequest($this->request)->run()) {
            return $this->fail($validate->getErrors()); 
        }

        $email = $this->request->getVar('email');
        $nohash = $this->request->getVar('password');
        $login = $modelAutentifikasi->where('email', $email)->orWhere('nohp', $email)->first();
        $hash = $login['password'];
        $sessionId = $this->sessionRandom();
        if (!password_verify($nohash, $hash)) {
            return $this->fail("password tidak sesuai");
        }
        
        $modelAutentifikasi->update($login['id'], ['session_id' => $sessionId]);
        $loginData = $modelAutentifikasi->where('email', $email)->orWhere('nohp', $email)->first();
        $accessData = json_decode($loginData['access']);
        helper('jwt');
        $response = [
            'message' => 'Otentikasi berhasil dilakukan',
            'data' => $loginData,
            'access' => $accessData,
            'Access_token' => createJWT($loginData['email'],$accessData,$sessionId)
        ];
        $modelLogs = new ModelLogs();
        $data = [
            'datetime' => date("Y-m-d H:i:s"),
            'user_id'  => $loginData['id'],
            'nama'  => $loginData['nama'],
            'email'  => $loginData['email'],
            'pengguna'  => $loginData['access'],
            'module'  => 'Login-Api',
            'ket'  => '',
            'jenis_aksi'  => $loginData['email'].'Log-in',
        ];
        $requestLogs = json_encode($data);
        $modelLogs->Logs($requestLogs, $data);
        return $this->respond($response);
    }

    public function handphone()
    {
        $modelAutentifikasi = $this->modelAutentifikasi;
        $validate = \config\Services::validation();
        $rules = [
            'handphone' => [
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'Silahkan Masukan No Handphone',
                    'numeric' => 'isi value dengan sesuai nomor anda',
                ]
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Silahkan Masukan Password'
                ]
            ],
        ];
        
        $validate->setRules($rules);
        if (!$validate->withRequest($this->request)->run()) {
            return $this->fail($validate->getErrors()); 
        }

        $handphone = $this->request->getVar('handphone');
        $nohash = $this->request->getVar('password');
        $login = $modelAutentifikasi->Where('nohp', $handphone)->first();
        if (empty($login)) {
            return $this->fail("No Hp tidak terdaftar");
        }

        // $encode = base64_encode('[{"id":"'.$random_data->id.'"},{"status":"'.$status.'"}]');
        
        // echo base_url().'/handphone_otp?code='.$encode;
        // $decode = base64_decode($encode);
        // $data = json_decode($decode, true);
        // var_dump($data[0]['id']);
        // die();
        $response = $this->sms->otp($login['nohp']);
        $encode = base64_encode('[{"id":"'.$login['id'].'"},{"handphone":"'.$login['nohp'].'"},{"request_id":"'.$response['request_id'].'"}]');
        $response['request_id'];
        $respon = [
            'message' => 'Berhasil Mengirimkan Otp, Silahkan Cek Hp Anda',
            'request_id' => $response['request_id'],
            'id_email' => $login['id'],
            'Nomor Handphone ' => $login['nohp'],
            'Link Redirect ' => base_url().'/autentikasi/handphone_otp/'.$encode
        ];
        return $this->respond($respon);
    }

    public function handphone_otp()
    {
        $request = \Config\Services::request();
        // echo $request->uri->getSegment(3);
        $decode = base64_decode($request->uri->getSegment(3));
        $otp = $this->request->getPost('otp');
        $data = json_decode($decode, true);
        // var_dump($data[0]['id']);
        $modelAutentifikasi = new ModelAutentikasi();
        $sessionId = $this->sessionRandom();
        $response = $this->sms->verifikasi($data[2]['request_id'],$data[1]['handphone'],$otp);
        $modelAutentifikasi->update($data[0]['id'], ['session_id' => $sessionId]);
        $loginData = $modelAutentifikasi->where('nohp', $data[1]['handphone'])->first();
        $accessData = json_decode($loginData['access']);
        helper('jwt');
        $response = [
            'message' => 'Berhasil Login Menggunakan Handphone',
            'data' => $loginData,
            'access' => $accessData,
            'Access_token' => createJWT($loginData['email'],$accessData,$sessionId)
        ];
        $modelLogs = new ModelLogs();
        $data = [
            'datetime' => date("Y-m-d H:i:s"),
            'user_id'  => $loginData['id'],
            'nama'  => $loginData['nama'],
            'email'  => $loginData['email'],
            'pengguna'  => $loginData['access'],
            'module'  => 'Login-Api',
            'ket'  => '',
            'jenis_aksi'  => $loginData['email'].'Log-in',
        ];
        $requestLogs = json_encode($data);
        $modelLogs->Logs($requestLogs, $data);
        return $this->respond($response);
    }

    public function Email()
    {
        $modelAutentifikasi = $this->modelAutentifikasi;
        $validate = \config\Services::validation();
        $rules = [
            'email' => [
                'rules' => 'required|Valid_email',
                'errors' => [
                    'required' => 'Silahkan Masukan Email',
                    'Valid_email' => 'Silahkan Masukan Email yang benar',
                ]
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Silahkan Masukan Password'
                ]
            ],
        ];
        
        $validate->setRules($rules);
        if (!$validate->withRequest($this->request)->run()) {
            return $this->fail($validate->getErrors()); 
        }

        $email = $this->request->getVar('email');
        $nohash = $this->request->getVar('password');
        $login = $modelAutentifikasi->where('email', $email)->first();
        $hash = $login['password'];
        $sessionId = $this->sessionRandom();
        if (!password_verify($nohash, $hash)) {
            return $this->fail("password tidak sesuai");
        }
        
        $modelAutentifikasi->update($login['id'], ['session_id' => $sessionId]);
        $loginData = $modelAutentifikasi->where('email', $email)->first();
        $accessData = json_decode($loginData['access']);
        helper('jwt');
        $response = [
            'message' => 'Otentikasi berhasil dilakukan',
            'data' => $loginData,
            'access' => $accessData,
            'Access_token' => createJWT($loginData['email'],$accessData,$sessionId)
        ];
        $modelLogs = new ModelLogs();
        $data = [
            'datetime' => date("Y-m-d H:i:s"),
            'user_id'  => $loginData['id'],
            'nama'  => $loginData['nama'],
            'email'  => $loginData['email'],
            'pengguna'  => $loginData['access'],
            'module'  => 'Login-Api',
            'ket'  => '',
            'jenis_aksi'  => $loginData['email'].'Log-in',
        ];
        $requestLogs = json_encode($data);
        $modelLogs->Logs($requestLogs, $data);
        return $this->respond($response);
    }

    public function sosmed()
    {
        $modelAutentifikasi = $this->modelAutentifikasi;
        $provider = 'Facebook';
        //getData With Sosial Media
        try{
            $hybridauth = new \Hybridauth\Hybridauth($this->hybridauth->provider());
            //Redirect ke halaman Sosmed yang ditentukan
            $adapter = $hybridauth->authenticate($provider);
            $userProfile = $adapter->getUserProfile();
            $sessionId = $this->sessionRandom();
            helper('jwt');
        }
        catch( Exception $e ){
            throw new \Exception($e);
        }
        $user_exist = $modelAutentifikasi->getProvider($provider, $userProfile->identifier);
    
        if( ! $user_exist )
        {
            //password login dengan sosmed di random
            $password_hash = password_hash(str_shuffle( "0123456789abcdefghijklmnoABCDEFGHIJ" ), PASSWORD_BCRYPT);

            //Simpan ke database untuk pendaftar baru
            try {
                $attr = [
                    'nama' => $userProfile->firstName.' '.$userProfile->lastName,
                    'email' => $userProfile->email,
                    'password' => $password_hash,
                    'access' => '{"Pemodal":"Editor"}',
                    'hybridauth_provider_name' => $provider,
                    'hybridauth_provider_uid' => $userProfile->identifier,
                ];
                $signUp = $this->db->table('api_login');
                $signUp->insert($attr);
            } catch (Exception $e) {
                throw new \Exception($e);
            }
            
            //SessionId Update
            $modelAutentifikasi->updateSession($userProfile->email,$sessionId);
            $response = [
                'message' => 'Selamat Anda telah Membuat akun menggunakan '.$provider,
                'data User' => $userProfile,
                'access' => json_decode('{"Pemodal":"Editor"}'),
                'Access_token' => createJWT($userProfile->email,json_decode('{"Pemodal":"Editor"}'),$sessionId)
            ];
            return $this->respond($response);
        }
        $modelAutentifikasi->updateSession($userProfile->email,$sessionId);
        $response = [
            'message' => 'Selamat Anda telah login menggunakan Akun'.$provider,
            'data User' => $userProfile,
            'access' => json_decode('{"Pemodal":"Editor"}'),
            'Access_token' => createJWT($userProfile->email,json_decode('{"Pemodal":"Editor"}'),$sessionId)
        ];
        return $this->respond($response);
    }

    //Membuat Session Random
    public function sessionRandom($length = 18)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    
}
