<?php

namespace App\Controllers\Pemodal;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Models\Pemodal\ModelPemodal;
use App\Models\ModelAutentikasi;
use App\Models\ModelLogs; 
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
class __construct extends Controller
{
    protected $request;

    protected $helpers = [];

    public $modelLogs;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);

        $header = $_SERVER["HTTP_AUTHORIZATION"];
        $key = getenv('JWT_SECRET_KEY');
        $encodedToken = getJWT($header);
        $decodedToken = JWT::decode($encodedToken, new Key($key, 'HS256'));
        if (empty($decodedToken->access->Pemodal)) {
            $pesan = 'Halaman Tidak Ditemukan';
            throw new \Exception($pesan);
        }
        $this->modelLogs = new ModelLogs();
        //Logs
        $data = [];
        $requestLogs= '' ;
        $this->modelLogs->Logs($requestLogs, $data);
    }
}
