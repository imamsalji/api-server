<?php

namespace App\Controllers\Pemodal;

use App\Controllers\Pemodal\__construct;
use CodeIgniter\API\ResponseTrait;
use App\Models\Pemodal\ModelPemodal;

class Refferal extends __construct
{
    use ResponseTrait; 

    public function index()
    {
        $response = [
            'message' => 'Halaman Refferal',
        ];
        return $this->respond($response);
    }
}