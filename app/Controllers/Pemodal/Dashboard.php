<?php

namespace App\Controllers\Pemodal;

use App\Controllers\Pemodal\__construct;
use CodeIgniter\API\ResponseTrait;
use App\Models\Pemodal\ModelPemodal;

class Dashboard extends __construct
{
    use ResponseTrait; 

    public function index()
    {
        $response = [
            'message' => 'Selamat datang di dashboard Pemodal',
        ];
        return $this->respond($response);
    }

    //Contoh Insert Logs dengan tbl_history_user
    public function create()
    {
        $data = [
            'datetime' => date("Y-m-d H:i:s"),
            'user_id'  => '118066953333',
            'nama'  => 'Imamsalji',
            'email'  => 'imamsalji7@gmail.com',
            'pengguna'  => 'Penggunahandal',
            'module'  => 'api',
            'ket'  => 'keterangan',
            'jenis_aksi'  => 'aksi',
        ];
        $requestLogs= '' ;

        $this->modelLogs->Logs($requestLogs, $data);
        $response = [
            'message' => $this->request->getPost('title')
        ];
        return $this->respond($response);
    }
}