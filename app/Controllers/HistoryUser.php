<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\ModelHistory;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\RequestTrait;

class HistoryUser extends Controller
{
    use ResponseTrait;

    public function history()
    {
        $modelhistory = new ModelHistory();
        $validate = \config\Services::validation();
        $rules = [
            'start_date' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Dari tanggal di isi',
                ]
            ],
            'end_date' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Sampai tanggal di isi'
                ]
            ],
        ];
        
        $validate->setRules($rules);
        if (!$validate->withRequest($this->request)->run()) {
            return $this->fail($validate->getErrors()); 
        }
        $start_date = $this->request->getPost('start_date');
        $end_date = $this->request->getPost('end_date');
        $data = $modelhistory->where('datetime BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"')->findAll(85);
        // $db      = \Config\Database::connect();
        // $builder = $db->table('tbl_history_user');
        // $builder->where('datetime BETWEEN 2021-06-22 and 2021-08-22')->get();
        // $modelhistory->where('datetime BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');

        $response = [
            'message' => $data,
        ];
        return $this->respond($response);
    }

    public function logs()
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('logs');
        $builder->where('time BETWEEN 2021-06-22 and 2021-08-22')->get(10);
        $response = [
            'message' => $builder,
        ];
        return $this->respond($response);
    }
}
