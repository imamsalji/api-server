<?php

namespace App\Controllers;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use CodeIgniter\Controller;
use App\Models\ModelLogs;
use App\Models\ModelAutentikasi;
use CodeIgniter\API\ResponseTrait;
use Exception;

class Logout extends Controller
{
    use ResponseTrait;
    public function index()
    {
        $header = $_SERVER["HTTP_AUTHORIZATION"];
        $key = getenv('JWT_SECRET_KEY');
        $encodedToken = getJWT($header);
        $decodedToken = JWT::decode($encodedToken, new Key($key, 'HS256'));
        $modelLogs = new ModelLogs();
        $modelAutentifikasi = new ModelAutentikasi();
        try {
            $logout = $modelAutentifikasi->where('email', $decodedToken->email)->first();
            $modelAutentifikasi->update($logout['id'], ['session_id' => '']);
            $data = [
                'datetime' => date("Y-m-d H:i:s"),
                'user_id'  => $logout['id'],
                'nama'  => $logout['username'],
                'email'  => $logout['email'],
                'pengguna'  => $logout['access'],
                'module'  => 'Logout-Api',
                'ket'  => '',
                'jenis_aksi'  => $logout['email'].'Log-out',
            ];
            $requestLogs = json_encode($data);
            $modelLogs->Logs($requestLogs, $data);
            $response = [
                'message' => 'Anda Berhasil Logout',
            ];
            return $this->respond($response);
        } catch (Exception $e) {
            throw new \Exception($e);
        }
        
    }
}
