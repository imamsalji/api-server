<?php

namespace App\Controllers\Fundadmin;

use App\Controllers\Fundadmin\__construct;
use CodeIgniter\API\ResponseTrait;
use App\Models\Fundadmin\ModelFundadmin;

class Dashboard extends __construct
{
    use ResponseTrait;
    public function index()
    {
        $response = [
            'message' => 'Selamat datang di dashboard Fundadmin',
        ];
        return $this->respond($response);
    }
}