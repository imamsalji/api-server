<?php

namespace App\Controllers\Operation;

use App\Controllers\Operation\__construct;
use CodeIgniter\API\ResponseTrait;
use App\Models\Operation\ModelOperation;

class Dashboard extends __construct
{
    use ResponseTrait;

    public function index()
    {
        $response = [
            'message' => 'Selamat datang di dashboard Operation',
        ];
        return $this->respond($response);
    }
}