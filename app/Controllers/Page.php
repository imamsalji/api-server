<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use DateTime;

class Page extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $db = \Config\Database::connect();
        //Jenis Efek
        if (empty($_GET['jenis'])) {
            $jenis_query = '';
        }else {
            $jenis_query = 'AND tbl_informasi_pinjaman.dm_pem_01001 = "'.$_GET['jenis'].'"';
        }

        //Sektor usaha
        if (empty($_GET['sektor'])) {
            $sektor_query = '';
        }else {
            $sektor_query = 'AND tbl_informasi_perusahaan.dm_pem_03012 = "'.$_GET['sektor'].'"';
        }

        //Sort By/Urutan dari
        if (empty($_GET['urutan'])) {
            $urutan_dari = '';
        }elseif ($_GET['urutan'] == 'Jumlah_Desc') {
            $urutan_dari = 'ORDER BY tbl_informasi_pinjaman.jml_pinjaman_terbit Desc';
        }elseif ($_GET['urutan'] == 'Jumlah_Asc') {
            $urutan_dari = 'ORDER BY tbl_informasi_pinjaman.jml_pinjaman_terbit Asc';
        }elseif ($_GET['urutan'] == 'Bagi_Desc') {
            $urutan_dari = 'ORDER BY tbl_informasi_pinjaman.bunga_persen Desc';
        }elseif ($_GET['urutan'] == 'Bagi_Asc') {
            $urutan_dari = 'ORDER BY tbl_informasi_pinjaman.bunga_persen Asc';
        }elseif ($_GET['urutan'] == 'Jangka_Desc') {
            $urutan_dari = 'ORDER BY tbl_informasi_pinjaman.dm_pem_02004 Desc';
        }elseif ($_GET['urutan'] == 'Jangka_Asc') {
            $urutan_dari = 'ORDER BY tbl_informasi_pinjaman.dm_pem_02004 Asc';
        }else {
            $urutan_dari ='';
        }
        $query_select = $db->query('SELECT *, tbl_informasi_pinjaman.pinjaman_id as pinjaman_id,tbl_informasi_pinjaman.user_peminjam_id as user_peminjam_id FROM tbl_informasi_pinjaman LEFT JOIN tbl_informasi_perusahaan ON 
        tbl_informasi_pinjaman.user_peminjam_id = tbl_informasi_perusahaan.user_peminjam_id WHERE tbl_informasi_pinjaman.status = "7"'.$jenis_query.''.$sektor_query.''.$urutan_dari.'');
        $card_prospektus = [];
        foreach ($query_select->getResult() as $row_pinjaman) {
        
            $progress = 0;
            
            //mencari kontribusi persen
            $queryKontribusi = $db->table('tbl_pendanaan')
                                ->selectSum('kontribusi_persen')
                                ->where('pinjaman_id', $row_pinjaman->pinjaman_id)
                                ->get();
            $queryKontribusiPersen = $queryKontribusi->getRow();
            if($queryKontribusiPersen->kontribusi_persen == NULL) {
                $kontribusi_persen = 0;
            } else {
                $kontribusi_persen = $queryKontribusiPersen->kontribusi_persen;
            }
            if($kontribusi_persen == '0.999') {
                $progress = 1;
            } elseif($kontribusi_persen > '1.000') {
                $progress = 1;
            } else {
                $progress = $kontribusi_persen;
            }
            date_default_timezone_set('Asia/Jakarta');
            // Tanggal Pinjaman Terbit
            $tgl_terbit = date_create($row_pinjaman->tgl_pinjaman_terbit);

            // Tanggal Akhir Penjualan (+45 Hari) diubah jadi (+30 hari)
            date_add($tgl_terbit, date_interval_create_from_date_string("32 days"));
            $tanggal_sell = date_format($tgl_terbit, "Y-m-d, H:i");

            if ($tanggal_sell <= date('Y-m-d, H:i')) {
            $status = "selesai";
            } else {
            $status = "";
            }
            // code prelisting Sesudah di ubah
            $proses = number_format($progress * 100, 0, '.', '.');
            $date1 = $row_pinjaman->tgl_pinjaman_terbit;
            $date = new DateTime($date1);
            $listing = $date->modify('+2 day');
            if ($listing->format("Y-m-d, H:i") >= date("Y-m-d, H:i") ) {
                $isPrelisting = 'PreListing';
            } else if ($proses == '100' || $status == "selesai") {
                $isPrelisting = 'PreListing';
            }else {
                $isPrelisting = 'Listing';
            }
            if (empty($_GET['status'])) {
                $status_prelisting = '';
            }else {
                $status_prelisting = $_GET['status'];
            }
            
            if ($isPrelisting == $status_prelisting || empty($status_prelisting)) {

                $JmlPendanaan = $db->table('tbl_pendanaan')
                                ->selectSum('jml_pendanaan')
                                ->where('pinjaman_id', $row_pinjaman->pinjaman_id)
                                ->get();
                $queryJmlPendanaan = $JmlPendanaan->getRow();
                if($queryJmlPendanaan->jml_pendanaan == NULL) {
                    $jml_pendanaan = 0;
                } else {
                    $jml_pendanaan = $queryJmlPendanaan->jml_pendanaan;
                }

                //mencari total yang mendanai
                $JmlPendanaantotal = $db->table('tbl_pendanaan')
                                ->select('COUNT(*) as total')
                                ->where('pinjaman_id', $row_pinjaman->pinjaman_id)
                                ->where('status !=', '')
                                ->get();
                $queryJmlPendanaan = $JmlPendanaantotal->getRow();
                $total_pendana = $queryJmlPendanaan->total;

                $InformasiPerusahaan = $db->table('tbl_informasi_perusahaan')
                                ->select('*')
                                ->where('user_peminjam_id', $row_pinjaman->user_peminjam_id)
                                ->get();
                $getInformasiPerusahaan = $InformasiPerusahaan->getRow();
                $nama_perusahaan = '';
                if (strtolower($getInformasiPerusahaan->dm_pem_03001) == 'limited company') {
                $nama_perusahaan = 'PT. ' . $getInformasiPerusahaan->dm_pem_03002;
                } else if (strtolower($getInformasiPerusahaan->dm_pem_03001) == 'commanditaire vennootschap (cv)') {
                $nama_perusahaan = 'CV. ' . $getInformasiPerusahaan->dm_pem_03002;
                } elseif (strtolower($getInformasiPerusahaan->dm_pem_03001)== 'cooperative') {
                $nama_perusahaan = 'Koperasi ' . $getInformasiPerusahaan->dm_pem_03002;
                } elseif (strtolower($getInformasiPerusahaan->dm_pem_03001) == 'yayasan') {
                $nama_perusahaan = 'Yayasan ' . $getInformasiPerusahaan->dm_pem_03002;
                } else {
                $nama_perusahaan = $getInformasiPerusahaan->dm_pem_03002;
                }

                $getFactsheetPinjaman = $db->table('tbl_factsheet_pinjaman')
                                        ->select('*')
                                        ->where('pinjaman_id', $row_pinjaman->pinjaman_id)
                                        ->get();
                if($getFactsheetPinjaman->getNumRows() > 0) {
                    $dataFactsheetPinjaman = $getFactsheetPinjaman->getRow();
                    if($dataFactsheetPinjaman->gbr_sektor_usaha != '') {
                        $gbr_sektor_usaha = 'https://scf.danamart.id/app/assets/uploads/'.$dataFactsheetPinjaman->gbr_sektor_usaha;
                    } else {
                        $gbr_sektor_usaha = 'https://scf.danamart.id/app/assets/images/no-picture.jpg';
                    }
                } else {
                    $gbr_sektor_usaha = 'https://scf.danamart.id/app/assets/images/no-picture.jpg';
                }

                $slot_pendanaan = $row_pinjaman->jml_pinjaman_terbit - $jml_pendanaan;

                $date1_status = date('Y-m-d');
                $date2_status = $row_pinjaman->tgl_pencairan_pinjaman;
                $disable = '';
                if($date2_status <= $date1_status) {
                    $disable = 'disabled';
                } else {
                    $disable = '';
                }
                // Tanggal Pinjaman Terbit
                $tgl_pinjaman_terbit = date_create($row_pinjaman->tgl_pinjaman_terbit);

                // Tanggal Akhir Penjualan (+45 Hari) di ubah menjadi 30 hari dan 2 hari masa pre-listing
                date_add($tgl_pinjaman_terbit, date_interval_create_from_date_string("32 days"));
                $date_sell = date_format($tgl_pinjaman_terbit, "Y-m-d, H:i");

                $today = date('Y-m-d, H:i');

                $start_date = new DateTime($today);
                $end_date = new DateTime($date_sell);
                if ($date_sell <= $today || number_format($progress * 100, 0, '.', '.') == '100') {
                    $batas_tanggal = "<span style='color:red;'>Pembelian Selesai</span>";
                } else {
                    $interval = $start_date->diff($end_date);
                    $batas_tanggal =  $interval->days . " Hari lagi";
                }
                $prospektus = 'https://scf.danamart.id/app/pemodal/prospektus/view/'.$row_pinjaman->pinjaman_id;
                $card = array(
                    'pinjaman_id' => $row_pinjaman->pinjaman_id,
                    'isPrelisting' => $isPrelisting,
                    'gbr_sektor_usaha' => $gbr_sektor_usaha,
                    'jml_pinjaman_terbit' => 'Rp. ' . number_format($row_pinjaman->jml_pinjaman_terbit, 0, ',', '.'),
                    'progress' => number_format($progress * 100, 0, '.', '.') . '%',
                    'batas_tanggal' => $batas_tanggal,
                    'jml_pendanaan' => 'Rp. ' . number_format($jml_pendanaan, 0, ',', '.'),
                    'total_pemodal' => $total_pendana .' Pemodal',
                    'nama_perusahaan' => $nama_perusahaan,
                    'deskripsi' => $getInformasiPerusahaan->dm_pem_03017,
                    'sektor' => $getInformasiPerusahaan->dm_pem_03012,
                    'lokasi' => $getInformasiPerusahaan->dm_pem_03008,
                    'prospektus' => $prospektus,
                );
                array_push($card_prospektus,$card);
            }
        }
        return $this->respond($card_prospektus);
    }
}
