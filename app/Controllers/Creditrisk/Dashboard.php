<?php

namespace App\Controllers\Creditrisk;

use App\Controllers\Creditrisk\__construct;
use CodeIgniter\API\ResponseTrait;
use App\Models\Creditrisk\ModelCreditrisk;

class Dashboard extends __construct
{
    use ResponseTrait;
    
    public function index()
    {
        $response = [
            'message' => 'Selamat datang di dashboard Creditrisk',
        ];
        return $this->respond($response);
    }
}