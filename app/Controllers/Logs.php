<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\ModelLogs;
use App\Models\ModelHistory;
use CodeIgniter\API\ResponseTrait;

class Logs extends Controller
{
    use ResponseTrait;

    public function user()
    {
        $modelLogs = new ModelLogs();
        $validate = \config\Services::validation();
        $rules = [
            'start_date' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Dari tanggal di isi',
                ]
            ],
            'end_date' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Sampai tanggal di isi'
                ]
            ],
        ];
        
        $validate->setRules($rules);
        if (!$validate->withRequest($this->request)->run()) {
            return $this->fail($validate->getErrors()); 
        }
        $start_date = $this->request->getPost('start_date');
        $end_date = $this->request->getPost('end_date');
        $data = $modelLogs->where('time BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"')->findAll(85);
        
        $response = [
            'message' => 'Menampilkan Logs Dari Tanggal : '.$start_date.' Sampai Tanggal',
            'data' => $data,
        ];
        return $this->respond($response);
    }

    public function history()
    {
        $modelhistory = new ModelHistory();
        $validate = \config\Services::validation();
        $rules = [
            'start_date' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Dari tanggal di isi',
                ]
            ],
            'end_date' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Sampai tanggal di isi'
                ]
            ],
        ];
        
        $validate->setRules($rules);
        if (!$validate->withRequest($this->request)->run()) {
            return $this->fail($validate->getErrors()); 
        }
        $start_date = $this->request->getPost('start_date');
        $end_date = $this->request->getPost('end_date');
        $data = $modelhistory->where('datetime BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"')->findAll(85);
        
        $response = [
            'message' => 'Menampilkan History User Dari Tanggal : '.$start_date.' Sampai Tanggal : '. $end_date,
            'data' => $data,
        ];
        return $this->respond($response);
    }
}
