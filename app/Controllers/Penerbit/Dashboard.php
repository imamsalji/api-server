<?php

namespace App\Controllers\Penerbit;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use App\Controllers\Penerbit\__construct;
use CodeIgniter\API\ResponseTrait;
use App\Models\ModelAutentikasi;

class Dashboard extends __construct
{
    use ResponseTrait;
    
    public function index()
    {
        $response = [
            'message' => 'Selamat datang di dashboard Penerbit',
        ];
        return $this->respond($response);
    }
}