<?php

namespace App\Controllers\Pengguna;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Models\ModelLogs; 
use App\Models\ModelAutentikasi;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class __construct extends Controller
{
    protected $request;

    protected $helpers = [];

    public $modelLogs;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);

        $modelAutentifikasi = new ModelAutentikasi();
        $header = $_SERVER["HTTP_AUTHORIZATION"];
        $key = getenv('JWT_SECRET_KEY');
        $encodedToken = getJWT($header);
        $decodedToken = JWT::decode($encodedToken, new Key($key, 'HS256'));
        $login = $modelAutentifikasi->where('email', $decodedToken->email)->first();
        if(empty($login['session_id'])){
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }elseif ($login['session_id'] != $decodedToken->sessionId) {
            $pesan = 'Akun ini telah digunakan di perangkat berbeda silahkan kembali ke halaman login';
            throw new \Exception($pesan);
        }elseif (empty($decodedToken->access->Pengguna)) {
            $pesan = 'Halaman Tidak Ditemukan';
            throw new \Exception($pesan);
        }
        $this->modelLogs = new ModelLogs();
        //Logs
        $data = [];
        $requestLogs= '' ;
        $this->modelLogs->Logs($requestLogs, $data);
    }
}
