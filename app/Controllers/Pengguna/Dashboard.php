<?php

namespace App\Controllers\Pengguna;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use App\Controllers\Fundadmin\__construct;
use CodeIgniter\API\ResponseTrait;
use App\Models\ModelAutentikasi;

class Dashboard extends __construct
{
    use ResponseTrait;
    
    public function index()
    {
        $response = [
            'message' => 'Selamat datang di dashboard Pengguna',
        ];
        return $this->respond($response);
    }
}