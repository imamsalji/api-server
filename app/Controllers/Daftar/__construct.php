<?php

namespace App\Controllers\Daftar;

use CodeIgniter\Controller;
use App\Libraries\Email;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Models\ModelAutentikasi;

class __construct extends Controller
{
    protected $request;

    public $template_mail;
    public $db;
    public $modelAutentifikasi;
    public $validate;

    protected $helpers = [];


    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        $this->template_mail = new Email;

        $this->db      = \Config\Database::connect();
        $this->modelAutentifikasi = new ModelAutentikasi();
        $this->validate = \config\Services::validation();
    }
}
