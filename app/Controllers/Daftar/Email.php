<?php

namespace App\Controllers\Daftar;

use App\Controllers\Daftar\__construct;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\Config\Services;
use Exception;
class Email extends __construct
{
    use ResponseTrait;
    public function index()
    {
        $rules = [
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Silahkan Masukan Email',
                ]
            ],
            'email' => [
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => 'Silahkan Masukan Email',
                    'valid_email' => 'Silahkan Masukan Format email dengan benar',
                    'is_unique' => 'Alamat Email sudah digunakan',
                ]
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Silahkan Masukan Password'
                ]
            ],
        ];
        
        $this->validate->setRules($rules);
        if (!$this->validate->withRequest($this->request)->run()) {
            return $this->fail($this->validate->getErrors()); 
        }

        $namaDepan = $this->request->getPost('namaDepan');
        $namaBlkng = $this->request->getPost('namaBlkng');
        $nama = $namaDepan.' '.$namaBlkng;
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $password_hash = password_hash($password, PASSWORD_BCRYPT);
        // User Create data
        $attr = [
            'nama' => $nama,
            'email' => $email,
            'password' => $password_hash,
            'access' => '{"Pemodal":"Editor"}',
        ];
        $this->modelAutentifikasi->insert($attr);
        $data = $this->modelAutentifikasi->where('email', $email)->first();;

        //Status Verifikasi Create
        $status_verif = [
            'api_login_id' => $data['id'],
            'email'  => $email,
            'nohp'  => '-',
            'status_email'  => '0',
            'status_nohp'  => '0',
            'datetime'  => date('Y-m-d H:i:s'),
        ];
        $verif = $this->db->table('api_status_verif');
        $verif->insert($status_verif);
        $encode = base64_encode('[{"id":"'.$data['id'].'"},{"nama":"'.$nama.'"},{"email":"'.$email.'"}]');

        //Send Email Untuk Verifikasi User
        $content = '
        <tr>
            <td class="container1" colspan="2">
            <p>
                <b>Halo ' . $nama . ',</b>
                <br><br>
                Terimakasih telah bergabung pada Danamart, platform layanan urun dana berbasis teknologi informasi. Sebentar lagi Anda dapat berkontribusi dalam investasi ramah lingkungan. Sebelum lanjut, silahkan konfirmasi email Anda terlebih dahulu.
                <br><br>
                [ <a href="'.base_url().'/daftar/email/verifikasi/'.$encode.'">Konfirmasi Email</a> ]
                <br><br>
                Jika terdapat pertanyaan mengenai produk/layanan Danamart, silahkan hubungi kami di 021 2555 6719.
            </p>
            </td>
        </tr>
        <tr>
            <td class="container1" colspan="2">
            <p>
                Salam,<br>
                <b>Danamart Team</b>
            </p>
            </td>
        </tr>';
        $htmlContent = $this->template_mail->template($content);
        $data_email['From'] = 'support@danamart.id';
        $data_email['To'] = $email;
        $data_email['Subject'] = '[Danamart] Verifikasi Akun Danamart';
        $data_email['HtmlBody'] = $htmlContent;
        $data_email['ReplyTo'] = 'support@danamart.id';
        $payload = json_encode($data_email);
        $curl_email = curl_init();
        curl_setopt($curl_email, CURLOPT_URL, "https://api.postmarkapp.com/email");
        curl_setopt($curl_email, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl_email, CURLOPT_ENCODING, "");
        curl_setopt($curl_email, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl_email, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl_email, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl_email, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl_email, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl_email, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "cache-control: no-cache", "X-Postmark-Server-Token: ed81f9b5-4fae-47ef-afcf-3709c2f12fa6"));
        $response2 = curl_exec($curl_email);
        curl_close($curl_email);
        $kirim_email = json_decode($response2, true);
        $response = [
            'message' => 'Akun Berhasil Di buat Cek Email Untuk Verifikasi Email atau bisa skip ke halaman pemodal',
            'redirect' => base_url().'/pemodal/dashboard',
        ];
        return $this->respond($response);
    }

    public function verifikasi()
    {
        $decode = base64_decode($this->request->uri->getSegment(4));
        $data = json_decode($decode, true);
        $id = $data[0]['id'];
        $nama = $data[1]['nama'];
        $email = $data[2]['email'];
        try{
            //Update Status Verifikasi User
            $query = $this->db->table('api_status_verif')->where('api_login_id',$id);
            $query->update([
                        'status_email' => '1'
                    ]);
            $response = [
                'message' => 'Selamat akun anda telah terverifikasi',
                'data' => $data,
                'status' => 'Suksess',
                'id' => $id,
                'nama' => $email,
                'email' => $nama,
            ];
            return $this->respond($response);
        } catch (Exception $e){
            return Services::response()->setJSON(
                [
                    'error' => $e->getMessage()
                ]
            );
        }
    }
}