<?php 

use App\Models\ModelAutentikasi;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

function getJWT($otentifikasiHeader){
    if (is_null($otentifikasiHeader)) {
        throw new Exception("Authentifikasi JWT gagal");
    }
    return explode(" ",$otentifikasiHeader)[1];
}
function validateJWT($encodedToken)
{
    $key = getenv('JWT_SECRET_KEY');
    $decodedToken = JWT::decode($encodedToken, new Key($key, 'HS256'));
    $modelOtentifikasi = new ModelAutentikasi();
    $modelOtentifikasi->getEmail($decodedToken->email);
    $login = $modelOtentifikasi->where('email', $decodedToken->email)->first();
    if(empty($login['session_id'])){
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }elseif ($login['session_id'] != $decodedToken->sessionId) {
        $pesan = 'Akun ini telah digunakan di perangkat berbeda silahkan kembali ke halaman login';
        throw new \Exception($pesan);
    }
}

function createJWT($email,$access,$sessionId)
{
    $waktuRequest = time();
    $waktuToken = getenv('JWT_TIME_TO_LIVE');
    $waktuExpire = $waktuRequest + $waktuToken;
    $payload = [
        'email' => $email,
        'access' => $access,
        'sessionId' => $sessionId,
        'time_in' => $waktuRequest,
        'time_exp' => $waktuExpire,
    ];
    $jwt = JWT::encode($payload, getenv('JWT_SECRET_KEY'), 'HS256');
    return $jwt;
}
?>