<?php 

namespace App\Libraries;

class smsotp{
    public function otp($nohp)
    {
        $awalan = substr($nohp,0,1);

        if ($awalan == '0') {
            $no_hp = '62'.substr($nohp,1);
        } else {
            $no_hp = $nohp;
        }

        // if ($provider_code == '9' || $provider_code == '3') {

        //     //call fungsi gabungan
        //     $this->load->library('fungsi_gabungan');
        //     //generate kode untuk user_id
        //     $stamp = date("Ymd");
        //     $random_id_length = 6;
        //     $rndid = $this->fungsi_gabungan->generateRandomString($random_id_length);
        //     $kode_otp = $rndid;


        //     $data_otp = ['kode_otp' => $kode_otp];

        //     $this->daftar_model->update_kode_otp($user_pendana_id, $data_otp);

        //     $zensiva_data = [
        //         'userkey' => '0rz39w',
        //         'passkey' => 'Syncmaster1987',
        //         'nohp' => $no_hp,
        //         'kode_otp' => $kode_otp,
        //     ];
        //     $curl = curl_init();

        //     curl_setopt_array($curl, array(
        //         CURLOPT_URL => "https://gsm.zenziva.net/api/sendOTP/",
        //         CURLOPT_RETURNTRANSFER => true,
        //         CURLOPT_ENCODING => "",
        //         CURLOPT_MAXREDIRS => 10,
        //         CURLOPT_TIMEOUT => 0,
        //         CURLOPT_FOLLOWLOCATION => true,
        //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //         CURLOPT_CUSTOMREQUEST => "POST",
        //         CURLOPT_POSTFIELDS => json_encode($zensiva_data),
        //         CURLOPT_HTTPHEADER => array(
        //             "Content-Type: application/json",

        //         ),
        //     ));

        //     $response = curl_exec($curl);

        //     curl_close($curl);

        // } else {
            // Script Kirim SMS Api NEXMO
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.nexmo.com/verify/json?api_key=14cb5367&api_secret=DHZanpwf8kVsn83w&number=" . $no_hp . "&brand=Danamart",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $json_decode = json_decode($response, true);

            return $json_decode;
        // }
    }

    public function verifikasi($request_id,$no_hp,$code)
    {
        // Script Kirim SMS Api NEXMO
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.nexmo.com/verify/check/json?api_key=14cb5367&api_secret=DHZanpwf8kVsn83w&request_id=".$request_id."&number=" . $no_hp . "&code=".$code."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $json_decode = json_decode($response, true);

        return $json_decode;
    }
}
?>