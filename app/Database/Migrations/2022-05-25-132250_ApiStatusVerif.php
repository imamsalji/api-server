<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ApiStatusVerif extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'api_login_id' => [
                'type' => 'INT',
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'nohp' => [
                'type' => 'VARCHAR',
                'constraint' => 13,
            ],
            'status_email' => [
                'type' => 'ENUM',
                'constraint' => ['0', '1'],
                'default' => '0',
            ],
            'status_nohp' => [
                'type' => 'ENUM',
                'constraint' => ['0', '1'],
                'default' => '0',
            ],
            'datetime' => [
                'type' => 'DATETIME',
            ],
            
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('api_status_verif');
    }

    public function down()
    {
        $this->forge->dropTable('api_status_verif');
    }
}
