<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ApiLogin extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'nama' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'unique' => true,
            ],
            'nohp' => [
                'type' => 'VARCHAR',
                'constraint' => 13,
                'unique' => true,
            ],
            'session_id' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'access' => [
                'type' => 'VARCHAR',
                'constraint' => 150
            ],
            
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('api_login');
    }

    public function down()
    {
        $this->forge->dropTable('api_login');
    }
}
