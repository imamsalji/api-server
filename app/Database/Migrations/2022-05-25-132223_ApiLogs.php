<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ApiLogs extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'uri' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'method' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'time' => [
                'type' => 'datetime',
            ],
            'response_code' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'request' => [
                'type' => 'VARCHAR',
                'constraint' => 150
            ],
            
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('api_logs');
    }

    public function down()
    {
        $this->forge->dropTable('api_logs');
    }
}
