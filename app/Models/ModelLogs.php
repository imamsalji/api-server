<?php 
namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ModelLogs extends Model
{
    protected $table = 'api_logs';
    protected $primaryKey = 'id';
    protected $allowedFields = ['uri','method','ip_address','time','response_code','request'];

    function Logs($requestLogs,$data)
    {
        
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $modelLogs = new ModelLogs();
        $response = [
            'uri' => $request->uri->getPath(),
            'method' => $request->getMethod(),
            'ip_address' => $request->getIPAddress(),
            'time' => date("Y-m-d H:i:s"),
            'response_code' => http_response_code(),
            'request' => $requestLogs,
        ];
        if ($modelLogs->save($response) == true) {
            if (!empty($data)) {
                $response = [
                    'uri' => $request->uri->getPath(),
                    'method' => $request->getMethod(),
                    'ip_address' => $request->getIPAddress(),
                    'time' => date("Y-m-d H:i:s"),
                    'response_code' => http_response_code(),
                    'request' => $requestLogs,
                ];
                $data['json_ipgeo'] = json_encode($response);
                if ($db->table('tbl_history_user')->insert($data)) {
                    return true;
                }else {
                    return false;
                }
            }
            return true;
        }else {
            return false;
        }
        

    }
}
    
?>