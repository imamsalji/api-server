<?php 
namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ModelHistory extends Model
{
    protected $table = 'tbl_history_user';
    protected $primaryKey = 'id';
    protected $allowedFields = ['datetime','user_id','nama','email','pengguna','module','ket','jenis_aksi','json_ipgeo'];

}
    
?>