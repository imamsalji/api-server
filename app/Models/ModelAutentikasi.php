<?php 
namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ModelAutentikasi extends Model
{
    protected $table = 'api_login';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nama','password','email','session_id','access'];

    function getEmail($email)
    {
        $builder = $this->table("api_login");
        $data = $builder->where('email',$email)->orWhere('nohp',$email)->first();
        if (!$data) {
            throw new Exception("Data Tidak Valid");
        }
        return $data;
    }

    function updateSession($email,$sessionId)
    {
        //Update Session
        try {
            $data = $this->table("api_login");
            $data->set('session_id', $sessionId);
            $data->where('email', $email);
            $data->update();
            return true;
        } catch (Exception $e) {
            throw new \Exception($e);
        }
    }

    function getProvider($provider_name, $provider_user_id)
    {
        $builder = $this->table("api_login");
        $data = $builder->where('hybridauth_provider_name',$provider_name)->Where('hybridauth_provider_uid',$provider_user_id)->first();
        if (!$data) {
            $data = [];
        }
        return $data;
    }
}
    
?>